<!-- Begin Home 6 -->
	<section class="home_6 wow fadeIn" data-wow-delay="0.5s">
		<div class="row expanded collapse">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'home_6' ); ?>
			</div>
		</div>
	</section>
<!-- End Home 6 -->