<!-- Begin Top -->
	<section class="top wow bounceInDown" data-wow-delay="0.5s">
		<div class="row align-justify align-middle">
			<div class="small-12 medium-3 columns">
				<?php dynamic_sidebar( 'logo' ); ?>
			</div>
			<div class="small-12 medium-9 columns">
				<div class="row align-center align-middle">
					<div class="small-12 medium-9 columns">
						<?php get_template_part( 'part', 'menu-top' ); ?>
					</div>
					<div class="small-12 medium-3 columns">
						<?php dynamic_sidebar( 'social_media' ); ?>
					</div>
				</div>
				<div class="clear"></div>
				<?php get_template_part( 'part', 'menu' ); ?>
			</div>
		</div>
	</section>
<!-- End Top -->