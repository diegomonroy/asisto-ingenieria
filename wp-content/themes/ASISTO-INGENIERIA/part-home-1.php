<!-- Begin Home 1 -->
	<section class="home_1 wow bounceInRight" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'home_1' ); ?>
			</div>
		</div>
	</section>
<!-- End Home 1 -->