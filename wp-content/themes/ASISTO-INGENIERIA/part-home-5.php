<!-- Begin Home 5 -->
	<section class="home_5 wow fadeIn" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'home_5' ); ?>
			</div>
		</div>
	</section>
<!-- End Home 5 -->