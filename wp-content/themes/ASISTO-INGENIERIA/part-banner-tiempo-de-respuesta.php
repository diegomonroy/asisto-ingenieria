<!-- Begin Banner -->
	<section class="banner wow bounceInUp" data-wow-delay="0.5s">
		<div class="row expanded collapse">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'banner_tiempo_de_respuesta' ); ?>
			</div>
		</div>
	</section>
<!-- End Banner -->