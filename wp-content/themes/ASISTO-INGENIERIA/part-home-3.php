<!-- Begin Home 3 -->
	<section class="home_3 wow bounceInDown" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'home_3' ); ?>
			</div>
		</div>
	</section>
<!-- End Home 3 -->