<?php

/*

Functions for my template

*/

/*
 * Function to add my styles files
 */
function my_styles_files() {
	wp_enqueue_style( 'foundation-float-css', get_template_directory_uri() . '/build/bower_components/foundation-sites/dist/css/foundation-float.min.css', false );
	wp_enqueue_style( 'animate-css', get_template_directory_uri() . '/build/bower_components/animate.css/animate.min.css', false );
	wp_enqueue_style( 'fancybox-css', get_template_directory_uri() . '/build/bower_components/fancybox/dist/jquery.fancybox.min.css', false );
	wp_enqueue_style( 'font-awesome-css', get_template_directory_uri() . '/build/bower_components/font-awesome/css/font-awesome.min.css', false );
	if ( is_child_theme() ) {
		wp_enqueue_style( 'parent-css', trailingslashit( get_template_directory_uri() ) . 'style.css', false );
	}
	wp_enqueue_style( 'theme-css', get_stylesheet_uri(), false );
}
add_action( 'wp_enqueue_scripts', 'my_styles_files' );

/*
 * Function to add my scripts files
 */
function my_scripts_files() {
	//wp_deregister_script( 'jquery' );
}
add_action( 'wp_enqueue_scripts', 'my_scripts_files' );

/*
 * Function to add my scripts files in footer
 */
function my_scripts_files_footer() {
	//wp_enqueue_script( 'jquery-js', get_template_directory_uri() . '/build/bower_components/jquery/dist/jquery.min.js', false );
	wp_enqueue_script( 'what-input-js', get_template_directory_uri() . '/build/bower_components/what-input/dist/what-input.min.js', false );
	wp_enqueue_script( 'foundation-js', get_template_directory_uri() . '/build/bower_components/foundation-sites/dist/js/foundation.min.js', false );
	wp_enqueue_script( 'fancybox-js', get_template_directory_uri() . '/build/bower_components/fancybox/dist/jquery.fancybox.min.js', false );
	wp_enqueue_script( 'wow-js', get_template_directory_uri() . '/build/bower_components/wow/dist/wow.min.js', false );
	wp_enqueue_script( 'theme-js', get_template_directory_uri() . '/build/app.js', false );
}
add_action( 'wp_footer', 'my_scripts_files_footer' );

/*
 * Function to register my menus
 */
function register_my_menus() {
	register_nav_menus(
		array(
			'main-menu' => __( 'Main Menu' ),
			'top-menu' => __( 'Top Menu' )
		)
	);
}
add_action( 'init', 'register_my_menus' );

/*
 * Function to add my custom menu item
 */
function my_custom_menu_item( $items, $args ) {
	if ( $args->theme_location == 'top-menu' ) {
		$items .= '<li><a href="https://www.facebook.com/Asisto-Ingenier%C3%ADa-491771237833410/" target="_blank"><i class="fa fa-facebook"></i></a></li>'; // Facebook
		$items .= '<li><a href="https://www.linkedin.com/company/asisto-ingeniería" target="_blank"><i class="fa fa-linkedin"></i></a></li>'; // LinkedIn
	}
	return $items;
}
//add_filter( 'wp_nav_menu_items', 'my_custom_menu_item', 10, 2 );

/*
 * Function to add theme support
 */
add_theme_support( 'post-thumbnails' );

/*
 * Function to register my sidebars and widgetized areas
 */
function arphabet_widgets_init() {
	register_sidebar(
		array(
			'name' => 'Logo',
			'id' => 'logo',
			'before_widget' => '<div class="moduletable_to1 text-center">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Social Media',
			'id' => 'social_media',
			'before_widget' => '<div class="moduletable_to4 text-center">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Inicio',
			'id' => 'banner_inicio',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Software Asisweb',
			'id' => 'banner_software_asisweb',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner PQR',
			'id' => 'banner_pqr',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Quiénes Somos',
			'id' => 'banner_quienes_somos',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Servicios',
			'id' => 'banner_servicios',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Mantenimiento Preventivo',
			'id' => 'banner_mantenimiento_preventivo',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Mantenimiento Correctivo',
			'id' => 'banner_mantenimiento_correctivo',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Mantenimiento Predictivo',
			'id' => 'banner_mantenimiento_predictivo',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Repotenciación de Unidades',
			'id' => 'banner_repotenciacion_de_unidades',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Alquiler de Instrumental Rotatorio',
			'id' => 'banner_alquiler_de_instrumental_rotatorio',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Montaje e Instalación de Equipos Odontológicos',
			'id' => 'banner_montaje_e_instalacion_de_equipos_odontologicos',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Capacitación',
			'id' => 'banner_capacitacion',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Metrología',
			'id' => 'banner_metrologia',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Laboratorios Móviles',
			'id' => 'banner_laboratorios_moviles',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Laboratorio Móvil de Instrumental Rotatorio',
			'id' => 'banner_laboratorio_movil_de_instrumental_rotatorio',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Laboratorio Móvil Electrónico',
			'id' => 'banner_laboratorio_movil_electronico',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Asesoría Visita de la Secretaría',
			'id' => 'banner_asesorias',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Asesoría en Licencias de Radiología',
			'id' => 'banner_asesoria_en_licencias_de_radiologia',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Cobertura Nacional',
			'id' => 'banner_cobertura_nacional',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Servicio a Domicilio',
			'id' => 'banner_servicio_a_domicilio',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Contáctenos',
			'id' => 'banner_contactenos',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Soporte Técnico',
			'id' => 'banner_soporte_tecnico',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Trabaja con Nosotros',
			'id' => 'banner_trabaja_con_nosotros',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Banner Tiempo de Respuesta',
			'id' => 'banner_tiempo_de_respuesta',
			'before_widget' => '<div class="moduletable_ba1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Service',
			'id' => 'service',
			'before_widget' => '<div class="moduletable_se1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Home 1',
			'id' => 'home_1',
			'before_widget' => '<div class="moduletable_h11">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Home 2',
			'id' => 'home_2',
			'before_widget' => '<div class="moduletable_h21">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Home 3',
			'id' => 'home_3',
			'before_widget' => '<div class="moduletable_h31">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Home 4',
			'id' => 'home_4',
			'before_widget' => '<div class="moduletable_h41">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Home 5',
			'id' => 'home_5',
			'before_widget' => '<div class="moduletable_h51">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Home 6',
			'id' => 'home_6',
			'before_widget' => '<div class="moduletable_h61">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Home 7',
			'id' => 'home_7',
			'before_widget' => '<div class="moduletable_h71">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Home 8',
			'id' => 'home_8',
			'before_widget' => '<div class="moduletable_h81">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Bottom',
			'id' => 'bottom',
			'before_widget' => '<div class="moduletable_bo1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
	register_sidebar(
		array(
			'name' => 'Pop Up',
			'id' => 'pop_up',
			'before_widget' => '<div class="moduletable_pop1">',
			'after_widget' => '</div>',
			'before_title' => '',
			'after_title' => ''
		)
	);
}
add_action( 'widgets_init', 'arphabet_widgets_init' );

/*
 * Custom shortcode to Service
 */
function shortcode_service() {
	if ( is_page( 'mantenimiento-preventivo' ) ) : $mantenimiento = 'active'; endif;
	if ( is_page( 'mantenimiento-correctivo' ) ) : $mantenimiento = 'active'; endif;
	if ( is_page( 'mantenimiento-predictivo' ) ) : $mantenimiento = 'active'; endif;
	if ( is_page( 'alquiler-de-instrumental-rotatorio' ) ) : $alquilerdeinstrumentalrotatorio = 'active'; endif;
	if ( is_page( 'metrologia' ) ) : $metrologia = 'active'; endif;
	if ( is_page( 'laboratorios-moviles' ) ) : $laboratoriosmoviles = 'active'; endif;
	if ( is_page( 'laboratorio-movil-electronico' ) ) : $laboratoriosmoviles = 'active'; endif;
	if ( is_page( 'laboratorio-movil-de-instrumental-rotatorio' ) ) : $laboratoriosmoviles = 'active'; endif;
	$html = '
		<div class="row" data-equalizer data-equalize-on="medium" id="home-1-eq">
			<div class="small-12 medium-3 columns">
				<p class="text-center"><a href="http://dev.amapolazul.com/asisto-ingenieria/servicios/metrologia"><img src="http://dev.amapolazul.com/asisto-ingenieria/wp-content/uploads/Home-1-1.png" title="METROLOGÍA" alt="METROLOGÍA"></a></p>
				<p class="text-center no-margin ' . $metrologia . '" data-equalizer-watch><a href="http://dev.amapolazul.com/asisto-ingenieria/servicios/metrologia">METROLOGÍA</a></p>
			</div>
			<div class="small-12 medium-3 columns">
				<p class="text-center"><a href="http://dev.amapolazul.com/asisto-ingenieria/servicios/mantenimiento-preventivo"><img src="http://dev.amapolazul.com/asisto-ingenieria/wp-content/uploads/Home-1-2.png" title="MANTENIMIENTO" alt="MANTENIMIENTO"></a></p>
				<p class="text-center no-margin ' . $mantenimiento . '" data-equalizer-watch><a href="http://dev.amapolazul.com/asisto-ingenieria/servicios/mantenimiento-preventivo">MANTENIMIENTO</a></p>
			</div>
			<div class="small-12 medium-3 columns">
				<p class="text-center"><a href="http://dev.amapolazul.com/asisto-ingenieria/servicios/laboratorios-moviles"><img src="http://dev.amapolazul.com/asisto-ingenieria/wp-content/uploads/Home-1-3.png" title="LABORATORIOS MÓVILES" alt="LABORATORIOS MÓVILES"></a></p>
				<p class="text-center no-margin ' . $laboratoriosmoviles . '" data-equalizer-watch><a href="http://dev.amapolazul.com/asisto-ingenieria/servicios/laboratorios-moviles">LABORATORIOS<br />MÓVILES</a></p>
			</div>
			<div class="small-12 medium-3 columns">
				<p class="text-center"><a href="http://dev.amapolazul.com/asisto-ingenieria/servicios/alquiler-de-instrumental-rotatorio"><img src="http://dev.amapolazul.com/asisto-ingenieria/wp-content/uploads/Home-1-4.png" title="ALQUILER DE INSTRUMENTAL ROTATORIO" alt="ALQUILER DE INSTRUMENTAL ROTATORIO"></a></p>
				<p class="text-center no-margin ' . $alquilerdeinstrumentalrotatorio . '" data-equalizer-watch><a href="http://dev.amapolazul.com/asisto-ingenieria/servicios/alquiler-de-instrumental-rotatorio">ALQUILER DE<br />INSTRUMENTAL<br />ROTATORIO</a></p>
			</div>
		</div>
	';
	return $html;
}
add_shortcode('service', 'shortcode_service');