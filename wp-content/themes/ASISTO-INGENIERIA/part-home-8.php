<!-- Begin Home 8 -->
	<section class="home_8 wow fadeIn" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'home_8' ); ?>
			</div>
		</div>
	</section>
<!-- End Home 8 -->