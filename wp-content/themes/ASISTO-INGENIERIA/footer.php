		<?php if ( is_front_page() ) : get_template_part( 'part', 'home-1' ); endif; ?>
		<?php if ( is_front_page() ) : get_template_part( 'part', 'home-2' ); endif; ?>
		<?php if ( is_front_page() ) : get_template_part( 'part', 'home-3' ); endif; ?>
		<?php if ( is_front_page() ) : get_template_part( 'part', 'home-4' ); endif; ?>
		<?php if ( is_front_page() ) : get_template_part( 'part', 'home-5' ); endif; ?>
		<?php if ( ! is_front_page() ) : get_template_part( 'part', 'home-8' ); endif; ?>
		<?php get_template_part( 'part', 'home-6' ); ?>
		<?php get_template_part( 'part', 'home-7' ); ?>
		<?php get_template_part( 'part', 'bottom' ); ?>
		<?php get_template_part( 'part', 'copyright' ); ?>
		<?php wp_footer(); ?>
		<?php if ( is_front_page() ) : ?>
		<!--<div id="hidden-content" style="max-width: 500px; display: none;">
			<?php /*dynamic_sidebar( 'pop_up' );*/ ?>
		</div>
		<a href="javascript:;" id="pop_up" data-fancybox data-options='{"src": "#hidden-content", "touch": false, "smallBtn": false}'></a>
		<script>
			jQuery( '#pop_up' ).trigger( 'click' );
		</script>-->
		<?php endif; ?>
	</body>
</html>