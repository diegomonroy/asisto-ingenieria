<!-- Begin Banner -->
	<section class="banner wow bounceInUp" data-wow-delay="0.5s">
		<div class="row expanded collapse">
			<div class="small-12 columns">
				<?php
				if ( is_page( 'servicios' ) ) : dynamic_sidebar( 'banner_servicios' ); endif;
				if ( is_page( 'mantenimiento-preventivo' ) ) : dynamic_sidebar( 'banner_mantenimiento_preventivo' ); endif;
				if ( is_page( 'mantenimiento-correctivo' ) ) : dynamic_sidebar( 'banner_mantenimiento_correctivo' ); endif;
				if ( is_page( 'mantenimiento-predictivo' ) ) : dynamic_sidebar( 'banner_mantenimiento_predictivo' ); endif;
				if ( is_page( 'repotenciacion-de-unidades' ) ) : dynamic_sidebar( 'banner_repotenciacion_de_unidades' ); endif;
				if ( is_page( 'alquiler-de-instrumental-rotatorio' ) ) : dynamic_sidebar( 'banner_alquiler_de_instrumental_rotatorio' ); endif;
				if ( is_page( 'montaje-e-instalacion-de-equipos-odontologicos' ) ) : dynamic_sidebar( 'banner_montaje_e_instalacion_de_equipos_odontologicos' ); endif;
				if ( is_page( 'capacitacion' ) ) : dynamic_sidebar( 'banner_capacitacion' ); endif;
				if ( is_page( 'metrologia' ) ) : dynamic_sidebar( 'banner_metrologia' ); endif;
				if ( is_page( 'laboratorios-moviles' ) ) : dynamic_sidebar( 'banner_laboratorios_moviles' ); endif;
				if ( is_page( 'laboratorio-movil-de-instrumental-rotatorio' ) ) : dynamic_sidebar( 'banner_laboratorio_movil_de_instrumental_rotatorio' ); endif;
				if ( is_page( 'laboratorio-movil-electronico' ) ) : dynamic_sidebar( 'banner_laboratorio_movil_electronico' ); endif;
				if ( is_page( 'asesoria-visita-de-la-secretaria' ) ) : dynamic_sidebar( 'banner_asesorias' ); endif;
				if ( is_page( 'asesoria-en-licencias-de-radiologia' ) ) : dynamic_sidebar( 'banner_asesoria_en_licencias_de_radiologia' ); endif;
				?>
			</div>
		</div>
	</section>
<!-- End Banner -->