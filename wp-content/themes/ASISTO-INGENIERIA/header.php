<!DOCTYPE html>
<html class="no-js" lang="<?php echo get_locale(); ?>">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="google-site-verification" content="gD-SX71R5aSMY9mv11RisJQ-l9783rxSe1oLXFF1px8">
		<title><?php bloginfo(title); ?></title>
		<!-- Begin Open Graph Protocol -->
		<meta property="og:url" content="<?php echo site_url(); ?>">
		<meta property="og:type" content="website">
		<meta property="og:title" content="<?php bloginfo(name); ?>">
		<meta property="og:description" content="<?php bloginfo(description); ?>">
		<meta property="og:image" content="<?php echo get_template_directory_uri(); ?>/build/logo_ogp.png">
		<link rel="image_src" href="<?php echo get_template_directory_uri(); ?>/build/logo_link_ogp.png">
		<!-- End Open Graph Protocol -->
		<?php wp_head(); ?>
		<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/build/favicon.ico">
		<!-- Begin Smartsupp -->
		<!-- Smartsupp Live Chat script -->
		<script type="text/javascript">
			var _smartsupp = _smartsupp || {};
			_smartsupp.key = 'f396323c26d23490a476d64b3dcff45ef45c8a1f';
			window.smartsupp||(function(d) {
				var s,c,o=smartsupp=function(){ o._.push(arguments)};o._=[];
				s=d.getElementsByTagName('script')[0];c=d.createElement('script');
				c.type='text/javascript';c.charset='utf-8';c.async=true;
				c.src='https://www.smartsuppchat.com/loader.js?';s.parentNode.insertBefore(c,s);
			})(document);
		</script>
		<!-- End Smartsupp -->
		<!-- Begin Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-108242234-1"></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());
			gtag('config', 'UA-108242234-1');
		</script>
		<!-- End Google Analytics -->
		<!-- Begin Google Tag Manager -->
		<script>
			(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
				new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
				j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
				'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
			})(window,document,'script','dataLayer','GTM-5F9M4SS');
		</script>
		<!-- End Google Tag Manager -->
		<!-- Begin Google Tag Manager (noscript) -->
		<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5F9M4SS" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
		<!-- End Google Tag Manager (noscript) -->
	</head>
	<body>
		<?php get_template_part( 'part', 'header' ); ?>