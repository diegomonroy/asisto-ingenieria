<!-- Begin Home 7 -->
	<section class="home_7 wow fadeInUp" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'home_7' ); ?>
			</div>
		</div>
	</section>
<!-- End Home 7 -->