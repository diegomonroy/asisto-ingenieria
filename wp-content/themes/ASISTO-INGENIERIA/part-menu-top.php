<!-- Begin Menu Top -->
	<div class="moduletable_to2">
		<?php
		wp_nav_menu(
			array(
				'menu_class' => 'menu',
				'container' => false,
				'theme_location' => 'top-menu',
				'items_wrap' => '<ul class="%2$s">%3$s</ul>'
			)
		);
		?>
	</div>
<!-- End Menu Top -->