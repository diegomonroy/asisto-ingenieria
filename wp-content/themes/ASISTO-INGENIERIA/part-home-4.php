<!-- Begin Home 4 -->
	<section class="home_4 wow bounceInUp" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<?php dynamic_sidebar( 'home_4' ); ?>
			</div>
		</div>
	</section>
<!-- End Home 4 -->