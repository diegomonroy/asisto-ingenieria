<?php
$style = '';
if ( is_front_page() ) :
	get_template_part( 'part', 'banner-inicio' );
	$style = 'content_grey';
endif;
if ( is_page( 'software-asisweb' ) ) :
	get_template_part( 'part', 'banner-software-asisweb' );
endif;
if ( is_page( 'pqr' ) ) :
	get_template_part( 'part', 'banner-pqr' );
endif;
if (
	is_page(
		array(
			'quienes-somos',
			'mision',
			'vision',
			'politicas'
		)
	)
) :
	get_template_part( 'part', 'banner-quienes-somos' );
endif;
if (
	is_page(
		array(
			'servicios',
			'mantenimiento-preventivo',
			'mantenimiento-correctivo',
			'mantenimiento-predictivo',
			'repotenciacion-de-unidades',
			'alquiler-de-instrumental-rotatorio',
			'montaje-e-instalacion-de-equipos-odontologicos',
			'capacitacion',
			'metrologia',
			'laboratorios-moviles',
			'laboratorio-movil-de-instrumental-rotatorio',
			'laboratorio-movil-electronico',
			'asesoria-visita-de-la-secretaria',
			'asesoria-en-licencias-de-radiologia',
		)
	)
) :
	get_template_part( 'part', 'banner-servicios' );
	//get_template_part( 'part', 'service' );
endif;
if ( is_page( 'cobertura-nacional' ) ) :
	get_template_part( 'part', 'banner-cobertura-nacional' );
endif;
if ( is_page( 'servicio-a-domicilio' ) ) :
	get_template_part( 'part', 'banner-servicio-a-domicilio' );
endif;
if ( is_page( 'contactenos' ) ) :
	get_template_part( 'part', 'banner-contactenos' );
endif;
if ( is_page( 'soporte-tecnico' ) ) :
	get_template_part( 'part', 'banner-soporte-tecnico' );
endif;
if ( is_page( 'trabaja-con-nosotros' ) ) :
	get_template_part( 'part', 'banner-trabaja-con-nosotros' );
endif;
if ( is_page( 'tiempo-de-respuesta' ) ) :
	get_template_part( 'part', 'banner-tiempo-de-respuesta' );
endif;
?>
<!-- Begin Content -->
	<section class="content <?php echo $style; ?> wow fadeIn" data-wow-delay="0.5s">
		<div class="row">
			<div class="small-12 columns">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
					<?php the_content(); ?>
				<?php endwhile; endif; ?>
			</div>
		</div>
	</section>
<!-- End Content -->